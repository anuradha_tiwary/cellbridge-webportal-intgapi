package cellbridge.customerlookup;

import io.netty.channel.ChannelOption;
import io.netty.handler.timeout.ReadTimeoutHandler;
import io.netty.handler.timeout.WriteTimeoutHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.client.reactive.ClientHttpConnector;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;
import reactor.netty.http.client.HttpClient;

/**
 *
 */
@Service
public class CustomerLookUpService {
    @Autowired
    private WebClient webClient;
    public static Logger logger = LoggerFactory.getLogger(CustomerLookUpService.class);
    @Value("${baseUrl}")
    private String baseUrl;
    @Value("${lookUpApiUrl}")
    private String lookUpApiUrl;

    /**
     * @return WebClient
     */
    @Bean
    public WebClient getWebClient() {
        logger.info("Entering getWebClient@CustomerLookUpService");
        ClientHttpConnector connector = getClientHttpConnector();
        logger.info("getting connector and building webclient");
        return WebClient.builder()
                .baseUrl(baseUrl)
                .clientConnector(connector)
                .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .build();
    }

    /**
     * @return
     */
    private ClientHttpConnector getClientHttpConnector() {
        logger.info("Entering getClientHttpConnector@CustomerLookUpService");
        HttpClient httpClient = HttpClient.create()
                .tcpConfiguration(client ->
                        client.option(ChannelOption.CONNECT_TIMEOUT_MILLIS, 10000)
                                .doOnConnected(conn -> conn
                                        .addHandlerLast(new ReadTimeoutHandler(10))
                                        .addHandlerLast(new WriteTimeoutHandler(10))));

        ClientHttpConnector connector = new ReactorClientHttpConnector(httpClient);
        logger.info("returning connector");
        return connector;
    }

    /**
     * @param query
     * @return
     */
    protected Mono<String> getCustomerLookUp(String query) {
        return getWebClient()
                .post()
                .uri(lookUpApiUrl)
                .body(Mono.just(query), String.class)
                .retrieve()
                .bodyToMono(String.class);
    }
}
