package cellbridge.customerlookup;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
public class CustomerLookUpController {

    @Autowired
    private CustomerLookUpService customerLookUpService;

    @RequestMapping(value = "/customer-login")
    private Mono<String> getCustomerDetail(@RequestBody String query) {
        return customerLookUpService.getCustomerLookUp(query);
    }

}
