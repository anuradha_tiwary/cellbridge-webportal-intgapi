package cellbridge.customerlookup;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 *
 */
@SpringBootApplication
public class CellbridgeApiApplication {

    /**
     *
     */
    private static final Logger log = LoggerFactory.getLogger(CellbridgeApiApplication.class);

    /**
     * @param args
     */
    public static void main(String[] args) {
        log.info("Entering main method : CellbridgeApiApplication start");
        SpringApplication.run(CellbridgeApiApplication.class, args);
    }

}
